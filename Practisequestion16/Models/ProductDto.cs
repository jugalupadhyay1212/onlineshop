﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Practisequestion16.Models
{
    public class ProductDto
    {
        public int Id { get; set; }

        public string ProductName { get; set; }

        public int CategoryId { get; set; }

        public int BrandId { get; set; }

        public decimal Qty { get; set; }

        public decimal Price { get; set; }

        public string Image_Path { get; set; }

        public HttpPostedFileBase Image { get; set; }

        //public string IsActive { get; set; }
        //public List<string>  check { get; set; }
    }
}