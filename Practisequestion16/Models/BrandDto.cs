﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Practisequestion16.Models
{
    public class BrandDto
    {
        public int Id { get; set; }

        public string BrandName { get; set; }
    }
}