﻿using Practisequestion16.Models;
using Practisequestion16.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Practisequestion16.Controllers
{
    
    public class ProductController : Controller
    {
        ProductService productService = new ProductService();
        CategoryService categoryService = new CategoryService();
        BrandService brandService = new BrandService();
        // GET: Product
        public ActionResult Product(int id = 0)
        {
            ViewBag.CateDrp = categoryService.GetCategoryListItem();
            ViewBag.BrandDrp = brandService.GetSelectListItems();
            if (id > 0)
            {
              
                return View(productService.GetById(id));
            }
            else
            {
                return View();
            }
        }
        
        [HttpPost]
        public ActionResult Product(ProductDto productDto)
        {
            if (productDto.Image.ContentLength > 0)
            {
                var fileName = Path.GetFileName(productDto.Image.FileName);
                var imageLocation = Path.Combine(Server.MapPath("~/App_Data/Pictures"), fileName);
                productDto.Image.SaveAs(imageLocation);
                productDto.Image_Path = imageLocation;
            }
            ViewBag.CateDrp = categoryService.GetCategoryListItem();
            ViewBag.BrandDrp = brandService.GetSelectListItems();
            productService.AddProduct(productDto);
            return View();

        }

        [HttpGet]
        public ActionResult GetAllProducts()
        {
            var data = productService.GetAllProducts();
            ViewBag.CateDrp = categoryService.GetCategoryListItem();
            ViewBag.BrandDrp = brandService.GetSelectListItems();
            return View(data);
        }
        [HttpGet]
        public ActionResult DeleteProduct(int id)
        {
            productService.DeleteProduct(id);
            return RedirectToAction("GetAllProducts");
        }
    }
}