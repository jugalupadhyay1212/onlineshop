﻿using Practisequestion16.Models;
using Practisequestion16.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Practisequestion16.Controllers
{
    
    public class CategoryController : Controller
    {
        CategoryService categoryService = new CategoryService();
        // GET: Category
        public ActionResult Category(int id = 0)
        {
            if (id > 0)
            {
                return View(categoryService.GetById(id));
            }
            else

            { return View(); }
        }
        
            
        
        [HttpPost]
        public ActionResult Category(CategoryDto categoryDto)
        {
            if (categoryDto.Id == 0)
            {
                categoryService.AddCategory(categoryDto);
            }
            else
            {
                categoryService.UpdateCategory(categoryDto);
            }
            
            return RedirectToAction("GetAllCategory");
        }
        
        [HttpGet]

        public ActionResult GetAllCategory()
        {
            var data = categoryService.GetCategoryList();
            return View(data);
        }
        [HttpGet]
        public ActionResult DeleteCategory(int id)
        {
            categoryService.DeleteCategory(id);
            return RedirectToAction("GetAllCategory");
        }
    }
}