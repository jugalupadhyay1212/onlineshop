﻿using Practisequestion16.Models;
using Practisequestion16.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Practisequestion16.Controllers
{
    public class LoginController : Controller
    {
        AccountServices accountServices = new AccountServices();
        // GET: Account
        
        public ActionResult Login()
        
        {
            return View();
        }
        [HttpPost]

        public ActionResult Login(AccountDto accountDto)
        {
            if (accountServices.Login(accountDto.Username, accountDto.Password))
            {
                return RedirectToAction("GetAllProducts", "Product",null);
            }
            
            return View();
        }

        public ActionResult Signup()
        {
            return View();
        }
        [HttpPost]

        public ActionResult Signup(AccountDto accountDto)
        {
            accountServices.AddUser(accountDto);
            return View();
        }

        [HttpGet]

        public ActionResult GetAllUsers()
        {
            var data = accountServices.GetAllUsers();
            return View(data);
        }



    }
}