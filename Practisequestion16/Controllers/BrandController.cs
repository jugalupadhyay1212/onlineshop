﻿using Practisequestion16.Models;
using Practisequestion16.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Practisequestion16.Controllers
{
    
    public class BrandController : Controller
    {
        BrandService brandService = new BrandService();
        // GET: Brand
        public ActionResult Brand(int  id = 0)
        {
            if (id > 0)
            {
                return View(brandService.GetById(id));
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult Brand(BrandDto brandDto)
        {
            if(brandDto.Id == 0)
            {
                brandService.AddBrand(brandDto);
            }
            else
            {
                brandService.UpdateBrand(brandDto);
            }
            return RedirectToAction("GetAllBrand");
        }
        
        [HttpGet]

        public ActionResult GetAllBrand()
        {
            var data = brandService.GetBrandList();
            return View(data);
        }
        [HttpGet]
        public ActionResult DeleteBrand(int id)
        {
            brandService.DeleteBrand(id);
            return RedirectToAction("GetAllBrand");
        }
    }
}