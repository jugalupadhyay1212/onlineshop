﻿using Practisequestion16.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;

namespace Practisequestion16.Service
{
    public class ProductService
    {
        EcommercedataEntities ProductEcom = new EcommercedataEntities();

        public void AddProduct(ProductDto product)
        {
            
            Producttable producttable = new Producttable();
            producttable.Id = product.Id;
            producttable.ProductName = product.ProductName;
            producttable.CategoryId = product.CategoryId;
            producttable.BrandId = product.BrandId;
            producttable.Qty = product.Qty;
            producttable.Price = product.Price;
            producttable.Images = product.Image_Path;
            ProductEcom.Producttables.Add(producttable);
            ProductEcom.SaveChanges();
        }

        public void EditProduct(ProductDto product)
        {
            Producttable producttable = ProductEcom.Producttables.FirstOrDefault(x => x.Id == product.Id);
            producttable.ProductName = product.ProductName;
            producttable.CategoryId= product.CategoryId;
            product.BrandId = product.BrandId;
            producttable.Qty = product.Qty;
            producttable.Price = product.Price;
            ProductEcom.Producttables.AddOrUpdate(producttable);
            ProductEcom.SaveChanges();
        }

        public List<ProductDto> GetAllProducts()
        {
            var data = ProductEcom.Producttables;
            List<ProductDto> List = new List<ProductDto>();

            foreach (var item in data)
            {
                List.Add(new ProductDto()
                {
                    Id = item.Id,
                    ProductName = item.ProductName,
                    CategoryId = item.CategoryId,
                    BrandId = item.BrandId,
                    Qty = item.Qty.Value,
                    Price = item.Price.Value,
                    Image_Path = item.Images,
                });
            }
            return List;
        }
        public ProductDto GetById(int id)
        {
            var data = ProductEcom.Producttables.FirstOrDefault(x => x.Id == id);
            return new ProductDto()
            {
                Id = data.Id,
                ProductName = data.ProductName,
                CategoryId = data.CategoryId,
                BrandId = data.BrandId,
                Qty = data.Qty.Value,
                Price = data.Price.Value
            };
        }
        public void DeleteProduct(int id)
        {
            var data = ProductEcom.Producttables.FirstOrDefault(x => x.Id == id);
            ProductEcom.Producttables.Remove(data);
            ProductEcom.SaveChanges();
        }
    }
}