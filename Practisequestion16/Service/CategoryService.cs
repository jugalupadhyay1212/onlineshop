﻿using Practisequestion16.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Practisequestion16.Service
{
    public class CategoryService
    {
        EcommercedataEntities CatEcom = new EcommercedataEntities();

        public void AddCategory(CategoryDto category)
        {
            Categorytable categorytable = new Categorytable();
            categorytable.Id = category.Id;
            categorytable.CategoryName = category.CategoryName;
            CatEcom.Categorytables.Add(categorytable);
            CatEcom.SaveChanges();
        }

        public void UpdateCategory(CategoryDto category)
        {
            Categorytable categorytable = CatEcom.Categorytables.FirstOrDefault(X => X.Id == category.Id);
            categorytable.Id = category.Id;
            categorytable.CategoryName = category.CategoryName;
            CatEcom.Categorytables.AddOrUpdate(categorytable);
            CatEcom.SaveChanges();
        }


        public List<SelectListItem> GetCategoryListItem()
        {
            //var data = CatEcom.Categorytables.ToList();
            //var data1 = data.Select(x => new SelectListItem()
            //{
            //    Text = x.CategoryName,
            //    Value = x.Id.ToString()
            //}).ToList();
            //return data1;
            return CatEcom.Categorytables.OrderByDescending(x => x.Id).Select(x => new SelectListItem()
            {
                Text = x.CategoryName,
                Value = x.Id.ToString()
            }).ToList();
        }

        public List<CategoryDto> GetCategoryList()
        {
            var data = CatEcom.Categorytables;
            List<CategoryDto> list = new List<CategoryDto>();

            foreach (var category in data)
            {
                list.Add(new CategoryDto()
                {
                    Id = category.Id,
                    CategoryName = category.CategoryName,
                });
            }
            return list;
        }

        public CategoryDto GetById(int id)
        {
            var data = CatEcom.Categorytables.FirstOrDefault(X => X.Id == id);
            return new CategoryDto() { Id = data.Id, CategoryName = data.CategoryName };
        }

        public void DeleteCategory(int id)
        {
            var data = CatEcom.Categorytables.FirstOrDefault(x => x.Id == id);
            CatEcom.Categorytables.Remove(data);
            CatEcom.SaveChanges();
        }
    }
}