﻿using Newtonsoft.Json.Linq;
using Practisequestion16.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using static System.Net.Mime.MediaTypeNames;

namespace Practisequestion16.Service
{
    public class BrandService
    {
        EcommercedataEntities BrandEcom = new EcommercedataEntities();

        public void AddBrand(BrandDto brand)
        {
            Brandtable brandtable = new Brandtable();
            brandtable.Id = brand.Id;
            brandtable.BrandName = brand.BrandName;
            BrandEcom.Brandtables.Add(brandtable);
            BrandEcom.SaveChanges();
        }

        public void UpdateBrand(BrandDto brand)
        {
            Brandtable brandtable = BrandEcom.Brandtables.FirstOrDefault(X => X.Id == brand.Id);
            brandtable.Id = brand.Id;
            brandtable.BrandName= brand.BrandName;
            BrandEcom.Brandtables.AddOrUpdate(brandtable);
            BrandEcom.SaveChanges();
        }

        public List<SelectListItem> GetSelectListItems()
        {
            return BrandEcom.Brandtables.OrderByDescending(x => x.Id).Select(x => new SelectListItem()
            {
                Text = x.BrandName,
                Value = x.Id.ToString()
            }).ToList();
        }


        public List<BrandDto> GetBrandList()
        {
            var data = BrandEcom.Brandtables;
            List<BrandDto> List = new List<BrandDto>();

            foreach (var item in data)
            {
                List.Add(new BrandDto()
                {
                    Id = item.Id,
                    BrandName = item.BrandName,
                });
            }
            return List;
        }

        public BrandDto GetById(int id)
        {
            var data = BrandEcom.Brandtables.FirstOrDefault(x => x.Id == id);
            return new BrandDto() { Id = data.Id, BrandName = data.BrandName };
        }
        public void DeleteBrand(int id)
        {
            var data = BrandEcom.Brandtables.FirstOrDefault(x => x.Id == id);
            BrandEcom.Brandtables.Remove(data);
            BrandEcom.SaveChanges();
        }
    }
}