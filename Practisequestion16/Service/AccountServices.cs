﻿using Practisequestion16.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Practisequestion16.Service
{
    public class AccountServices
    {
        EcommercedataEntities AccEcom = new EcommercedataEntities();

        public void AddUser(AccountDto account)
        {
            Account account1 = new Account();
            account1.Id = account.Id;
            account1.FirstName = account.FirstName;
            account1.LastName = account.LastName;
            account1.Username = account.Username;
            account1.Password = account.Password;
            AccEcom.Accounts.Add(account1);
            AccEcom.SaveChanges();
        }

        public List<AccountDto> GetAllUsers()
        {
            var data = AccEcom.Accounts;
            List<AccountDto> list = new List<AccountDto>();

            foreach (var item in data)
            {
                list.Add(new AccountDto()
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Username = item.Username,
                    Password = item.Password
                });
            }
            return list;
        }

        public bool Login(string userName, string password)
        {
            return AccEcom.Accounts.Any(x => x.Username == userName && x.Password == password);

        }
    }
}